const jwt = require('jsonwebtoken');

const passcode = 'plazaDiamant';

// Authentication for users logging in

module.exports.createAccessToken = (user) => {

	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, passcode, {});
};

// Verification

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization;

	if(typeof token !== 'undefined'){

		token = token.slice(7, token.length);
		return jwt.verify(token, passcode, (err, data) => {

			if(err) {
				return response.send({auth: 'Failed. Please double-check token.'});
			} else {
				next();
			}
		})
	} else {
		return response.send({auth: 'Failed. Please double-check token.'});
	}
};

// Decryption

module.exports.decode = (token) => {
	
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, passcode, (error, data) => {
			if(error){
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else {
		return null;
	}
};