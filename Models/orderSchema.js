const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "Please include purchasing user details."]
	},
	products: [{
		productId: {
			type: String,
			required: [true, "Please indicate ordered product."]
		},
		quantity: {
			type: Number,
			default: 1
		}
	}],
	address: {
		type: String,
		required: [true, "Please indicate where you would like the delivery to go."]
	},
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);