const mongoose = require('mongoose');
const Order = require('../Models/orderSchema.js');
const auth = require('../auth.js');

// Creating new order
/*
	Business Logic:
		1. Only users w/o admin access can create orders.
		2. An order must contain the ff:
			- userId of registered customer making order
			- list of ordered products (array of objects: indicate product ID and quantity of item)
			- user's address
			- total of order

	Possible user-end errors:
		1. User is an admin
		2. Invalid product ID

	Possible code bugs:
		1. Non-existent product ID gets added into order anyway
		2. totalAmount does not automatically and precisely reflect the total price of the order
*/

module.exports.checkout = (request, response) => {

}